Make sure you have all the necessary google sdk libaries installed. Install Android SDK (version 23) 
Install the Android Support Libary and the Google Repository as well from the sdk. 

Make sure that you have virtual environments enabled on your machine to speed up the android emulator: 
http://techtach.com/2014/05/boost-android-emulator-performanceon-linux-speeding-up-android-emulator-on-ubuntu/

Make also sure to install Java 8:
http://tecadmin.net/install-oracle-java-8-jdk-8-ubuntu-via-ppa/

Your Ionic project is ready to go! Some quick tips:

* cd into your project:
```
cd ionFullApp
```

* Setup this project to use Sass:
```
ionic setup sass
```

* Develop in the browser with live reload:
```
ionic serve
```

* Add a platform (ios or Android):
```
ionic platform add ios [android]
```

Note: iOS development requires OS X currently
See the Android Platform Guide for full Android installation instructions:
https://cordova.apache.org/docs/en/edge/guide_platforms_android_index.md.html

* Build your app:
```
ionic build <PLATFORM>
```

* Simulate your app:
```
ionic emulate <PLATFORM>
```

* Run your app on a device:
```
ionic run <PLATFORM>
```

* Package an app using Ionic package service:
```
ionic package <MODE> <PLATFORM>
```

For more help use ```ionic --help``` or visit the Ionic docs: http://ionicframework.com/docs


You can find the documentation here: http://bit.ly/ionicthemes-ionfullapp

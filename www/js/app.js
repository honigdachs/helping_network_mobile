// Ionic Starter App

angular.module('underscore', [])
.factory('_', function() {
  return window._; // assumes underscore has already been loaded on the page
});

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('your_app_name', [
  'ionic',
  'angularMoment',
  'LocalStorageModule',
  'your_app_name.controllers',
  'your_app_name.directives',
  'your_app_name.filters',
  'your_app_name.services',
  'your_app_name.factories',
  'your_app_name.config',
  'your_app_name.views',
  'underscore',
  'ngMap',
  'ngResource',
  'ngCordova',
  'slugifier',
  'ionic.contrib.ui.tinderCards',
  'youtube-embed'
])

.run(function($ionicPlatform, PushNotificationsService, $rootScope, $ionicConfig, $timeout, $state){

  $rootScope.userLocation = {};

  $ionicPlatform.on("deviceready", function(){
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }

    PushNotificationsService.register();
  });

  // This fixes transitions for transparent background views
  $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){
    if(toState.name.indexOf('auth.walkthrough') > -1)
    {
      // set transitions to android to avoid weird visual effect in the walkthrough transitions
      $timeout(function(){
        $ionicConfig.views.transition('android');
        $ionicConfig.views.swipeBackEnabled(false);
      	console.log("setting transition to android and disabling swipe back");
      }, 0);
    }
  });
  $rootScope.$on("$stateChangeSuccess", function(event, toState, toParams, fromState, fromParams){
    if(toState.name.indexOf('app.feeds-categories') > -1)
    {
      // Restore platform default transition. We are just hardcoding android transitions to auth views.
      $ionicConfig.views.transition('platform');
      // If it's ios, then enable swipe back again
      if(ionic.Platform.isIOS())
      {
        $ionicConfig.views.swipeBackEnabled(true);
      }
    	console.log("enabling swipe back and restoring transition to platform default", $ionicConfig.views.transition());
    }
  });

  $rootScope.$on('loginRequired', function() {
    $state.go('auth.walkthrough');
    //$location.path('/auth/walkthrough');
  });

  $ionicPlatform.on("resume", function(){
    PushNotificationsService.register();
  });

})


.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider, $httpProvider) {
  $httpProvider.defaults.withCredentials = true;
  // this interceptor catches 401 responses from rest calls to the server and redirects the user to the login page
  // if needed.
  $httpProvider.interceptors.push('SecurityHttpInterceptor');
  $ionicConfigProvider.tabs.position('bottom'); // other values: top


  $stateProvider


  //INTRO
  .state('auth', {
    url: "/auth",
    templateUrl: "views/auth/auth.html",
    abstract: true,
    controller: 'AuthCtrl'
  })

  .state('auth.walkthrough', {
    cache: false,
    url: '/walkthrough',
    templateUrl: "views/auth/walkthrough.html",
    controller: 'HelpingNetworkLoginController'
  })

  .state('auth.login', {
    url: '/login',
    templateUrl: "views/auth/login.html",
    controller: 'LoginCtrl'
  })

  .state('auth.signup', {
    url: '/signup',
    templateUrl: "views/auth/signup.html",
    controller: 'SignupCtrl'
  })

  .state('auth.forgot-password', {
    url: "/forgot-password",
    templateUrl: "views/auth/forgot-password.html",
    controller: 'ForgotPasswordCtrl'
  })

  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "views/app/tab-menu.html",
    controller: 'AppCtrl'
  })

  .state('app.maps', {
    url: "/miscellaneous/maps",
    views: {
      'menuContent': {
        templateUrl: "views/app/miscellaneous/maps.html",
        controller: 'MapsCtrl'
      }
    }
  })


  .state('app.around', {
    url: '/around',
    params: {
      transition: null
    },
    views: {
      'tab-around': {
        templateUrl: 'views/app/miscellaneous/maps.html',
        controller: 'MapsCtrl',
        resolve: {
          'helpRequestData' : function($http){
            return $http.get('https://helping.network/get_all_help_requests_sorted_by_distance?location=false')
                .then(function(response){
                  return response.data;
                });
          }
        }
      }
    }
  })

  .state('app.around-list', {
    url: '/around-list',
    params: {
      transition: null
    },
    views: {
      'tab-around': {
        templateUrl: 'views/app/help-request-list.html',
        controller: 'AroundListCtrl',
          resolve: {
              'helpRequestData' : function($http){
                  return $http.get('https://helping.network/get_all_help_requests_sorted_by_distance?location=true')
                      .then(function(response){
                          return response.data;
                      });
              }
          }

      }
    }
  })

  .state('app.help-request', {
      url: '/help-request',
      params: {
          transition: null
      },
      views: {
          'tab-around': {
              templateUrl: 'views/app/help-request.html',
              controller: 'HelpRequestCtrl'
          }
      }
  })






  .state('app.chats', {
    url: '/chats',
    views: {
      'tab-chats': {
        templateUrl: 'views/app/chats.html',
        controller: 'ChatsCtrl'
      }
    }
  })

  .state('app.profile', {
    url: '/profile',
    views: {
      'tab-profile': {
        templateUrl: 'views/app/profile.html',
        controller: 'ProfileController',
        resolve: {
          'profileData' : function($http){
            return $http.get('https://helping.network/profile')
                .then(function(response){
                  console.log('returning response!', response.data);
                  return response.data;
                });
          }
        }



      }
    }
  })


  //MISCELLANEOUS
  /*
   .state('app.miscellaneous', {
   url: "/miscellaneous",
   views: {
   'menuContent': {
   templateUrl: "views/app/miscellaneous/miscellaneous.html"
   }
   }
   })
   */

      /*
      .state('app.profile', {
        url: "/profile",
        views: {
          'menuContent': {
            templateUrl: "views/app/profile.html",
            controller: 'ProfileController',
            resolve: {
              'profileData' : function($http){
                return $http.get('https://helping.network/profile')
                    .then(function(response){
                      console.log('returning response!');
                      return response.data;
                    });
              }
            }

          }
        }
      }) */



      /*
       .state('app.chats', {
       url: "/chats",
       views: {
       'menuContent': {
       templateUrl: "views/app/chats.html",
       controller: 'ChatsCtrl'
       }
       }
       })


       .state('app.chat-detail', {
       url: '/chats/:chatId',
       views: {
       'tab-chats': {
       templateUrl: 'views/app/chat-detail.html',
       controller: 'ChatDetailCtrl'
       }
       }
       })


       .state('app.image-picker', {
       url: "/miscellaneous/image-picker",
       views: {
       'menuContent': {
       templateUrl: "views/app/miscellaneous/image-picker.html",
       controller: 'ImagePickerCtrl'
       }
       }
       })

       //LAYOUTS
       .state('app.layouts', {
       url: "/layouts",
       views: {
       'menuContent': {
       templateUrl: "views/app/layouts/layouts.html"
       }
       }
       })

       .state('app.tinder-cards', {
       url: "/layouts/tinder-cards",
       views: {
       'menuContent': {
       templateUrl: "views/app/layouts/tinder-cards.html",
       controller: 'TinderCardsCtrl'
       }
       }
       })

       .state('app.slider', {
       url: "/layouts/slider",
       views: {
       'menuContent': {
       templateUrl: "views/app/layouts/slider.html"
       }
       }
       })

       //FEEDS
       .state('app.feeds-categories', {
       url: "/feeds-categories",
       views: {
       'menuContent': {
       templateUrl: "views/app/feeds/feeds-categories.html",
       controller: 'FeedsCategoriesCtrl'
       }
       }
       })

       .state('app.category-feeds', {
       url: "/category-feeds/:categoryId",
       views: {
       'menuContent': {
       templateUrl: "views/app/feeds/category-feeds.html",
       controller: 'CategoryFeedsCtrl'
       }
       }
       })

       .state('app.feed-entries', {
       url: "/feed-entries/:categoryId/:sourceId",
       views: {
       'menuContent': {
       templateUrl: "views/app/feeds/feed-entries.html",
       controller: 'FeedEntriesCtrl'
       }
       }
       })

       //WORDPRESS
       .state('app.wordpress', {
       url: "/wordpress",
       views: {
       'menuContent': {
       templateUrl: "views/app/wordpress/wordpress.html",
       controller: 'WordpressCtrl'
       }
       }
       })

       .state('app.post', {
       url: "/wordpress/:postId",
       views: {
       'menuContent': {
       templateUrl: "views/app/wordpress/wordpress_post.html",
       controller: 'WordpressPostCtrl'
       }
       },
       resolve: {
       post_data: function(PostService, $ionicLoading, $stateParams) {
       $ionicLoading.show({
       template: 'Loading post ...'
       });

       var postId = $stateParams.postId;
       return PostService.getPost(postId);
       }
       }
       })

       //OTHERS
       .state('app.settings', {
       url: "/settings",
       views: {
       'menuContent': {
       templateUrl: "views/app/settings.html",
       controller: 'SettingsCtrl'
       }
       }
       })

       .state('app.forms', {
       url: "/forms",
       views: {
       'menuContent': {
       templateUrl: "views/app/forms.html"
       }
       }
       })

  .state('app.bookmarks', {
    url: "/bookmarks",
    views: {
      'menuContent': {
        templateUrl: "views/app/bookmarks.html",
        controller: 'BookMarksCtrl'
      }
    }
  })
  */
  //$ionicConfigProvider.views.maxCache(0);

;

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/auth/walkthrough');
});

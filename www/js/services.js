angular.module('your_app_name.services', [])

.service('FeedList', function ($rootScope, FeedLoader, $q){
	this.get = function(feedSourceUrl) {
		var response = $q.defer();
		//num is the number of results to pull form the source
		FeedLoader.fetch({q: feedSourceUrl, num: 20}, {}, function (data){
			response.resolve(data.responseData);
		});
		return response.promise;
	};
})


// PUSH NOTIFICATIONS
.service('PushNotificationsService', function ($rootScope, $cordovaPush, NodePushServer, GCM_SENDER_ID){
	/* Apple recommends you register your application for push notifications on the device every time it’s run since tokens can change. The documentation says: ‘By requesting the device token and passing it to the provider every time your application launches, you help to ensure that the provider has the current token for the device. If a user restores a backup to a device other than the one that the backup was created for (for example, the user migrates data to a new device), he or she must launch the application at least once for it to receive notifications again. If the user restores backup data to a new device or reinstalls the operating system, the device token changes. Moreover, never cache a device token and give that to your provider; always get the token from the system whenever you need it.’ */
	this.register = function() {
		var config = {};

		// ANDROID PUSH NOTIFICATIONS
		if(ionic.Platform.isAndroid())
		{
			config = {
				"senderID": GCM_SENDER_ID
			};

			$cordovaPush.register(config).then(function(result) {
				// Success
				console.log("$cordovaPush.register Success");
				console.log(result);
			}, function(err) {
				// Error
				console.log("$cordovaPush.register Error");
				console.log(err);
			});

			$rootScope.$on('$cordovaPush:notificationReceived', function(event, notification) {
				console.log(JSON.stringify([notification]));
				switch(notification.event)
				{
					case 'registered':
						if (notification.regid.length > 0 ) {
							console.log('registration ID = ' + notification.regid);
							NodePushServer.storeDeviceToken("android", notification.regid);
						}
						break;

					case 'message':
						if(notification.foreground == "1")
						{
							console.log("Notification received when app was opened (foreground = true)");
						}
						else
						{
							if(notification.coldstart == "1")
							{
								console.log("Notification received when app was closed (not even in background, foreground = false, coldstart = true)");
							}
							else
							{
								console.log("Notification received when app was in background (started but not focused, foreground = false, coldstart = false)");
							}
						}

						// this is the actual push notification. its format depends on the data model from the push server
						console.log('message = ' + notification.message);
						break;

					case 'error':
						console.log('GCM error = ' + notification.msg);
						break;

					default:
						console.log('An unknown GCM event has occurred');
						break;
				}
			});

			// WARNING: dangerous to unregister (results in loss of tokenID)
			// $cordovaPush.unregister(options).then(function(result) {
			//   // Success!
			// }, function(err) {
			//   // Error
			// });
		}

		if(ionic.Platform.isIOS())
		{
			config = {
				"badge": true,
				"sound": true,
				"alert": true
			};

			$cordovaPush.register(config).then(function(result) {
				// Success -- send deviceToken to server, and store for future use
				console.log("result: " + result);
				NodePushServer.storeDeviceToken("ios", result);
			}, function(err) {
				console.log("Registration error: " + err);
			});

			$rootScope.$on('$cordovaPush:notificationReceived', function(event, notification) {
				console.log(notification.alert, "Push Notification Received");
			});
		}
	};
})


// BOOKMARKS FUNCTIONS
.service('BookMarkService', function (_, $rootScope){

	this.bookmarkFeedPost = function(bookmark_post){

		var user_bookmarks = !_.isUndefined(window.localStorage.ionFullApp_feed_bookmarks) ?
														JSON.parse(window.localStorage.ionFullApp_feed_bookmarks) : [];

		//check if this post is already saved

		var existing_post = _.find(user_bookmarks, function(post){ return post.link == bookmark_post.link; });

		if(!existing_post){
			user_bookmarks.push({
				link: bookmark_post.link,
				title : bookmark_post.title,
				date: bookmark_post.publishedDate,
				excerpt: bookmark_post.contentSnippet
			});
		}

		window.localStorage.ionFullApp_feed_bookmarks = JSON.stringify(user_bookmarks);
		$rootScope.$broadcast("new-bookmark");
	};

	this.bookmarkWordpressPost = function(bookmark_post){

		var user_bookmarks = !_.isUndefined(window.localStorage.ionFullApp_wordpress_bookmarks) ?
														JSON.parse(window.localStorage.ionFullApp_wordpress_bookmarks) : [];

		//check if this post is already saved

		var existing_post = _.find(user_bookmarks, function(post){ return post.id == bookmark_post.id; });

		if(!existing_post){
			user_bookmarks.push({
				id: bookmark_post.id,
				title : bookmark_post.title,
				date: bookmark_post.date,
				excerpt: bookmark_post.excerpt
			});
		}

		window.localStorage.ionFullApp_wordpress_bookmarks = JSON.stringify(user_bookmarks);
		$rootScope.$broadcast("new-bookmark");
	};

	this.getBookmarks = function(){
		return {
			feeds : JSON.parse(window.localStorage.ionFullApp_feed_bookmarks || '[]'),
			wordpress: JSON.parse(window.localStorage.ionFullApp_wordpress_bookmarks || '[]')
		};
	};
})


// WP POSTS RELATED FUNCTIONS
.service('PostService', function ($rootScope, $http, $q, WORDPRESS_API_URL){

	this.getRecentPosts = function(page) {
		var deferred = $q.defer();

		$http.jsonp(WORDPRESS_API_URL + 'get_recent_posts/' +
		'?page='+ page +
		'&callback=JSON_CALLBACK')
		.success(function(data) {
			deferred.resolve(data);
		})
		.error(function(data) {
			deferred.reject(data);
		});

		return deferred.promise;
	};


	this.getPost = function(postId) {
		var deferred = $q.defer();

		$http.jsonp(WORDPRESS_API_URL + 'get_post/' +
		'?post_id='+ postId +
		'&callback=JSON_CALLBACK')
		.success(function(data) {
			deferred.resolve(data);
		})
		.error(function(data) {
			deferred.reject(data);
		});

		return deferred.promise;
	};

	this.shortenPosts = function(posts) {
		//we will shorten the post
		//define the max length (characters) of your post content
		var maxLength = 500;
		return _.map(posts, function(post){
			if(post.content.length > maxLength){
				//trim the string to the maximum length
				var trimmedString = post.content.substr(0, maxLength);
				//re-trim if we are in the middle of a word
				trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf("</p>")));
				post.content = trimmedString;
			}
			return post;
		});
	};

	this.sharePost = function(link){
		window.plugins.socialsharing.share('Check this post here: ', null, null, link);
	};

})

.factory('SecurityHttpInterceptor', function($q, $rootScope) {
	return {

		// optional method
		'responseError': function(rejection) {
			// do something on error
			console.log('got response error! ', rejection);
			if (rejection.status === 401) {
				//DO WHAT YOU WANT
				console.log('SECURITYHTTPINTERCEPTOR I will redirect the user to the current location now:');
				// no valid server side cookie exists, therefore lets delete it also at client side
				//$cookieStore.remove('loggedIn');
				console.log('HTTP INTERCEPTOR');
				$rootScope.$emit('loginRequired');

			}
			//else{
				// usually security errors are raised because of login issues:
			//	console.log('SECURITYHTTPINTERCEPTOR I will redirect the user to the current location now:');
				// no valid server side cookie exists, therefore lets delete it also at client side
				//$cookieStore.remove('loggedIn');
			//	$rootScope.$emit('loginRequired');
			//}
			return $q.reject(rejection);
		}


	};
})

.factory('appRedirectService', function($state) {
	return {
		goToState: function (stateName, transition) {
			$state.go(stateName, transition);


		}
	};
})

.factory('xsrfCookieFactory', function(){
	// this method does some black magic found in the tornado documentation and returns the cookie object of
	// the current user. add _xsrf : "the value of this method" to any post request for the server to avoid xsrf errors.
	return{
		getXsrfCookie : function(name){
			var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
			if (!r && name == "_xsrf"){
				return 'not_found';
			}
			return r ? r[1] : undefined;
		}
	};
})

.service('ChatInteractionService', function ($http, xsrfCookieFactory){

	this.deleteHelpRequest = function (helpRequestId){
		var xsrfToken = xsrfCookieFactory.getXsrfCookie('_xsrf');
		var postData = {withCredentials: true};
		var config = {
			params: {

				'helpRequestId': helpRequestId,
				'_xsrf': xsrfToken
			}
		};
		return  $http.post('/delete_help_request', postData, config);
	};

	this.markHelpRequestAsInappropriate = function(helpRequestId){
		var xsrfToken = xsrfCookieFactory.getXsrfCookie('_xsrf');
		var postData = {withCredentials: true};
		var config = {
			params: {

				'helpRequestId': helpRequestId,
				'_xsrf': xsrfToken
			}
		};
		return $http.post('/mark_help_request', postData, config);
	};


	this.checkStatusOfHelpRequest = function(helpRequestId){
		return $http.get('/get_help_request_status/' + helpRequestId);
	};

	this.pickUserForHelpRequest = function(userId, helpRequestId){
		var xsrfToken = xsrfCookieFactory.getXsrfCookie('_xsrf');

		var postData = {withCredentials: true};
		var config = {
			params: {
				'helpRequestId': helpRequestId,
				'userId': userId,
				'_xsrf': xsrfToken
			}
		};

		return $http.post('/pick_user_for_help_request', postData, config);

	};


	this.checkIfUserIsHelpRequestCreator = function(){
		return $http.get("/profile");
	};


	this.saveConversationToProfile = function(helpRequestId){

		var xsrfToken = xsrfCookieFactory.getXsrfCookie('_xsrf');
		var postData = {withCredentials: true};
		var config = {
			params: {
				'helpRequestId': helpRequestId,
				'_xsrf': xsrfToken
			}
		};
		return $http.post('/save_conversation_to_profile', postData, config);
	};

	this.loadHelpRequestInformation= function(helpRequestId){
		return $http.get('https://helping.network/get_help_request_details/' + helpRequestId);
	};



	this.sendHelpOffer = function(helpRequestId){
		var xsrfToken = xsrfCookieFactory.getXsrfCookie('_xsrf');
		var postData = {withCredentials: true};
		var config = {
			params: {
				'helpRequestId': helpRequestId,
				'_xsrf': xsrfToken
			}
		};
		return $http.post('/offer_help_to_user', postData, config);
	};



});

myApp.controller('userInfo', function ($scope, $state, $localStorage, userDataManagement) {

        $scope.Add = function () {
            var userData = {'name': $scope.name, 'dob': $scope.dob, 'interest': $sope.interest};
            $localStorage.savedInfo = userData;
            userDataManagement.postData(userData, error);
        };
        $scope.update = function () {
            var userData2 = {'name': $scope.name, 'dob': $scope.dob, 'interest': $scope.interest};
            $localStorage.updateInfo = userData2;
            userDataManagement.postData(userData2, error);
        };
    }
);





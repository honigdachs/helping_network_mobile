angular.module('your_app_name.controllers', [])

.controller('AuthCtrl', function($scope, $ionicConfig) {

})

// APP
.controller('AppCtrl', function($scope, $ionicConfig) {

})


.controller('ProfileController', function($scope, $http, $window, $state, profileData){
    $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
        //viewData.enableBack = true; enables back button
    });


	this.profileData = profileData;
	console.log('visited the profile page..', profileData);
	$scope.firstName = profileData.userdetails.first_name;
	$scope.lastName = profileData.userdetails.last_name;
	$scope.aboutMeText = profileData.userdetails.about_me_text;
	$scope.userId = profileData.userdetails.userid.$oid;
	$scope.karmaPoints = profileData.userdetails.karma;
	console.log('loaded profile');
	$scope.flipItem = false;

	/*
	 initProfile = function(){
	 $http.get('https://helping.network/profile')
	 .success(function(data, status, headers, config) {
	 console.log('profile data', data);
	 if (data.userdetails) {
	 = data.userdetails.karma;


	 // succefull login
	 //alert('log auth check successfull');
	 //$state.go('app.maps');
	 //User.isLogged = true;
	 //User.username = data.username;
	 //LoginCookieService.setCookie(data.username, data.email, data.user_id);
	 // the user is just logging in and not authenticating for the first time:
	 }
	 })
	 .error(function(data, status, headers, config) {
	 //alert('auth check failed');
	 //$state.go('auth.walkthrough');


	 //User.isLogged = false;
	 //User.username = '';
	 //alert('not logged in dude');
	 // the 3 parameter is only added because the url is defined like this in the routeprovider
	 //$location.path('/login/'+ 3);
	 });
	 };
	 */
	$scope.getNumber = function(num) {
		// arrays initialize at zero that is why we put -1 here
		return new Array(num);
	};

	$scope.makeFlip = function(){
		$scope.flipItem = true;

	};

})






//LOGIN
.controller('LoginCtrl', function($scope, $state, $templateCache, $q, $rootScope) {
	$scope.doLogIn = function(){
		$state.go('app.feeds-categories');
	};

	$scope.user = {};

	$scope.user.email = "john@doe.com";
	$scope.user.pin = "12345";

	// We need this for the form validation
	$scope.selected_tab = "";

	$scope.$on('my-tabs-changed', function (event, data) {
		$scope.selected_tab = data.title;
	});

})


// LOGIN
.controller('HelpingNetworkLoginController', function($scope, $http, $window, $state){
	$scope.hasSuccessfullyLoggedIn = false;

	//$scope.$watch("hasSuccessfullyLoggedIn", function() {
	//	console.log('successfull auth!');
	//	if ($scope.hasSuccessfullyLoggedIn === true){
	//		$state.go('app.maps');
	//	}

//		},  true);

	$scope.openHelpingNetworkGoogleSignIn = function(){
		//var ref = window.open('https://www.google.at', '_blank', 'location=yes');
		//ref.addEventListener('loadstart', function() { alert(event.url); });
		console.log('trying to sign in...');
		var ref = window.open('https://helping.network/auth/login/google', '_blank', 'location=no');
		ref.addEventListener('loadstop', function(event) {
			if(event.url.indexOf("updateLoginCredentials") > -1) {

				$scope.hasSuccessfullyLoggedIn = true;
				console.log('closing the browser!');
				$state.go('app.around');
				ref.close();
				//$state.go('app.maps');
			}
		});

	};

			$scope.openHelpingNetworkFacebookSignIn = function(){
		//var ref = window.open('https://www.google.at', '_blank', 'location=yes');
		//ref.addEventListener('loadstart', function() { alert(event.url); });
		console.log('trying to sign in...');
		var ref = window.open('https://helping.network/auth/login/facebook', '_blank', 'location=no');
		ref.addEventListener('loadstop', function(event) {
			if(event.url.indexOf("updateLoginCredentials") > -1) {

				$scope.hasSuccessfullyLoggedIn = true;
				console.log('closing the browser!');
				$state.go('app.around');
				ref.close();
				//$state.go('app.maps');
			}
		});

	};
})

//LOGOUT
.controller('HelpingNetworkLogOutController', function($scope, $http, $window, $state){
	var performServerLogout = function(){

		$http.get('https://helping.network/auth/logout')
			.success(function(data, status, headers, config) {
				console.log('logged out successfully');
				$state.go('auth.walkthrough');
			})
			.error(function(data, status, headers, config) {
				console.log('could not log out for some reasen server side error...');
				$state.go('auth.walkthrough');
			});

	};
	performServerLogout();

})


	.controller('SignupCtrl', function($scope, $state, $http) {
		$scope.user = {};

		$http.get('https://helping.network/check_if_authenticated')
			.success(function(data, status, headers, config) {
				if (data.status) {
					// succefull login
					alert('log auth check successfull');
					//$state.go('app.maps');
					//User.isLogged = true;
					//User.username = data.username;
					//LoginCookieService.setCookie(data.username, data.email, data.user_id);
					// the user is just logging in and not authenticating for the first time:
				}
			})
			.error(function(data, status, headers, config) {
				console.log('auth check failed reason:', status);
			});
		/*
		 $http.get('https://helping.network/profile')
		 .success(function(data, status, headers, config) {
		 if (data) {
		 // succefull login
		 console.log('got the profile', data);
		 alert('got the profile');
		 //User.isLogged = true;
		 //User.username = data.username;
		 //LoginCookieService.setCookie(data.username, data.email, data.user_id);
		 // the user is just logging in and not authenticating for the first time:
		 }
		 })
		 .error(function(data, status, headers, config) {
		 console.log('could not get the profile');

		 //User.isLogged = false;
		 //User.username = '';
		 //alert('not logged in dude');
		 // the 3 parameter is only added because the url is defined like this in the routeprovider
		 //$location.path('/login/'+ 3);
		 });
		 */

		$scope.user.email = "john2@doe.com";

		$scope.doSignUp = function(){
			$state.go('app.feeds-categories');
		};
	})

.controller('ForgotPasswordCtrl', function($scope, $state) {
	$scope.recoverPassword = function(){
		$state.go('app.feeds-categories');
	};

	$scope.user = {};
})

.controller('RateApp', function($scope) {
	$scope.rateApp = function(){
		if(ionic.Platform.isIOS()){
			//you need to set your own ios app id
			AppRate.preferences.storeAppURL.ios = '1234555553>';
			AppRate.promptForRating(true);
		}else if(ionic.Platform.isAndroid()){
			//you need to set your own android app id
			AppRate.preferences.storeAppURL.android = 'market://details?id=ionFB';
			AppRate.promptForRating(true);
		}
	};
})


.controller('SendMailCtrl', function($scope, $cordovaEmailComposer, $ionicPlatform) {
  //we use email composer cordova plugin, see the documentation for mor options: http://ngcordova.com/docs/plugins/emailComposer/
  $scope.sendMail = function(){
    $ionicPlatform.ready(function() {
      $cordovaEmailComposer.isAvailable().then(function() {
        // is available
        console.log("Is available");
        $cordovaEmailComposer.open({
          to: 'hi@startapplabs.com',
          subject: 'Nice Theme!',
  				body:    'How are you? Nice greetings from IonFullApp'
        }).then(null, function () {
          // user cancelled email
        });
      }, function () {
        // not available
        console.log("Not available");
      });
    });
  };
})

.controller('MapsCtrl', function($scope, $ionicLoading, $stateParams, $rootScope, $interval, helpRequestData) {

	$rootScope.userLocation = {latitude: helpRequestData.coordinates_of_user[0], longitude: helpRequestData.coordinates_of_user[1]};

	$scope.makeFlip = false;
	if($stateParams.transition == 'makeFlip'){
		$scope.makeFlip = true;
	}

	$scope.info_position = {
		lat: 43.07493,
		lng: -89.381388
	};

	$scope.center_position = {
		lat: 43.07493,
		lng: -89.381388
	};

	$scope.my_location = "";

	$scope.$on('mapInitialized', function(event, map) {
		$scope.map = map;
		var mapCenter = new google.maps.LatLng(helpRequestData.coordinates_of_user[0], helpRequestData.coordinates_of_user[1]);
		$scope.map.setCenter(mapCenter);
		$scope.GenerateMapMarkers();


	});

	// $scope.map .. this exists after the map is initialized
	var markers = [];
	console.log('my help request data is: ', helpRequestData);
	for (var i=0; i<helpRequestData.help_request_list.length ; i++) {
		markers[i] = new google.maps.Marker({
			title: helpRequestData.help_request_list[i].title,
			icon: 'https://helping.network/marker_picture/' + i
		});
	}

	$scope.GenerateMapMarkers = function() {
		$scope.date = Date(); // Just to show that we are updating

		var numMarkers = Math.floor(Math.random() * 4) + 4;  // betwween 4 & 8 of them
		var existingLocations = [];
		for (i = 0; i < helpRequestData.help_request_list.length; i++) {
			var existingLatitude = false;
			var existinglongitude = false;
			for (var index = 0; index < existingLocations.length; index++) {
				if (existingLocations[index].latitude == helpRequestData.help_request_list[i].latitude) {
					existingLatitude = true;
					if (existingLocations[index].longitude == helpRequestData.help_request_list[i].longitude) {
						existinglongitude = true;
					}
				}
			}
			if (existingLatitude && existinglongitude) {

				var randomInteger = Math.floor(Math.random() * 101);

				// we use a random number for the moving locations as well to make the positioning
				// of the markers truly random:

				var randomMovingInteger = Math.floor(Math.random() * 15);
				var movingFloat = Math.random()/10;
				var movedLatitude = null;
				var movedLongitude = null;
				if (randomInteger % 2 === 0) {
					movedLatitude = helpRequestData.help_request_list[i].latitude - 0.0001 + movingFloat;
					movedLongitude = helpRequestData.help_request_list[i].longitude - 0.0001 + movingFloat;
				} else {
					movedLatitude = helpRequestData.help_request_list[i].latitude + 0.0001 + movingFloat;
					movedLongitude = helpRequestData.help_request_list[i].longitude + 0.0001 + movingFloat;
				}
				var movedLatLong = new google.maps.LatLng(movedLatitude, movedLongitude);
				markers[i].setPosition(movedLatLong);
				markers[i].setMap($scope.map);



			} else {
				var lat =   helpRequestData.help_request_list[i].latitude;
				var lng = helpRequestData.help_request_list[i].longitude;
				// You need to set markers according to google api instruction
				// you don't need to learn ngMap, but you need to learn google map api v3
				// https://developers.google.com/maps/documentation/javascript/markers
				var latlng = new google.maps.LatLng(lat, lng);
				markers[i].setPosition(latlng);
				markers[i].setMap($scope.map);

				var newMarkerEntry = {'latitude': lat, 'longitude': lng};
				existingLocations.push(newMarkerEntry);
			}

		}
		var markerCluster = new MarkerClusterer($scope.map, markers, {imagePath: 'https://github.com/googlemaps/js-marker-clusterer/blob/gh-pages/images/m1.png?raw=true'});

	};



	/*


	$scope.centerOnMe= function(){

		$scope.positions = [];

		$ionicLoading.show({
			template: 'Loading...'
		});

		// with this function you can get the user’s current position
		// we use this plugin: https://github.com/apache/cordova-plugin-geolocation/
		navigator.geolocation.getCurrentPosition(function(position) {
			// todo try to get the location through the ip first and then try to ask for the actual location

			$rootScope.userLocation = {latitude: position.coords.latitude, longitude: position.coords.longitude};

			var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
			$scope.current_position = {lat: position.coords.latitude, lng: position.coords.longitude};
			$scope.my_location = position.coords.latitude + ", " + position.coords.longitude;
			$scope.map.setCenter(pos);

			$ionicLoading.hide();
		}, function(err) {
				 // error
				$ionicLoading.hide();
		});
	};

	//$scope.centerOnMe();
	*/
})

.controller('HelpRequestCtrl', function($scope, $stateParams, $rootScope){


})

.controller('AroundListCtrl', function($scope, $stateParams, helpRequestData, $rootScope, $ionicModal,
									   ChatInteractionService) {
	// With the new view caching in Ionic, Controllers are only called
	// when they are recreated or on app start, instead of every page change.
	// To listen for when this page is active (for example, to refresh data),
	// listen for the $ionicView.enter event:
	//
	$scope.$on('$ionicView.enter', function(e) {
        $scope.helpRequestList = helpRequestData.help_request_list;
        $scope.currentHrToShow = null;
        $scope.currentPublicWallMessagesToShow = [];
        $scope.activeUsersInPublicWall = {};
        $scope.activeUsersInPublicWallList = [];
        $scope.navBarPadding = '0px';
        $scope.showPublicWallConverstationView = false;

    });


    $ionicModal.fromTemplateUrl('views/app/help-request.html', {
        scope: $scope,
        animation: 'slide-in-up',
    }).then(function(modal) {
        $scope.modal = modal;
    });

    var loadChatHistoryAndActiveUsers = function (savedMessages) {
        // updating the active user list
        for (var i = 0; i < savedMessages.length; i++) {
            if (!Object.prototype.hasOwnProperty.call($scope.activeUsersInPublicWall, JSON.parse(savedMessages[i]).email)) {
                // this is the singleton object which saves the current users
                $scope.activeUsersInPublicWall[JSON.parse(savedMessages[i]).email] = JSON.parse(savedMessages[i]).from;
                $scope.activeUsersInPublicWallList.push({
                    'userName': JSON.parse(savedMessages[i]).from,
                    'email': JSON.parse(savedMessages[i]).email,
                    'userId': JSON.parse(savedMessages[i]).userId
                });
            }
            // loading the chat history
            $scope.currentPublicWallMessagesToShow.push(JSON.parse(savedMessages[i]));
        }
    };


    $scope.openModal = function(helpRequest) {
    	console.log('help request info:', helpRequest);
    	$scope.currentHrToShow = helpRequest;
        $scope.modal.show();
        ChatInteractionService.loadHelpRequestInformation(helpRequest.id).then(function (helpRequestInformation) {
            loadChatHistoryAndActiveUsers(helpRequestInformation.data.chat_history.saved_messages);
            console.log('got the following public wall conversation: ',$scope.currentPublicWallMessagesToShow);
        });
    };

    $scope.closeModal = function() {
        $scope.modal.hide();
        $scope.currentPublicWallMessagesToShow = [];
    };

    //Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
        $scope.modal.remove();
    });

    // Execute action on hide modal
    $scope.$on('modal.hidden', function() {
        // Execute action
    });

    // Execute action on remove modal
    $scope.$on('modal.removed', function() {
        // Execute action
    });
    $scope.imageSource = function (userId) {
    	console.log('showing :', userId);
        return 'https://helping.network/public_picture/' + userId;
    };

    $scope.ShowPublicConversation = function(){
    	$scope.showPublicWallConverstationView = true;
        $scope.navBarPadding = '43px';

    };
    $scope.goBackToHelpRequestDescription = function () {
        $scope.showPublicWallConverstationView = false;
        $scope.navBarPadding = '0px';

    };



    $rootScope.userLocation = {latitude: helpRequestData.coordinates_of_user[0], longitude: helpRequestData.coordinates_of_user[1]};
	$scope.userCurrentLocation = $rootScope.userLocation;


	$scope.makeFlip = false;
	if($stateParams.transition == 'makeFlip'){
		$scope.makeFlip = true;
	}



	$scope.getDistanceOfHelpRequestToUserinKm = function(lat1,lon1,lat2,lon2){
		//console.log('getting: ', lat1, lon1, lat2, lon2);
			var R = 6371; // Radius of the earth in km
			var dLat = deg2rad(lat2-lat1);  // deg2rad below
			var dLon = deg2rad(lon2-lon1);
			var a =
					Math.sin(dLat/2) * Math.sin(dLat/2) +
					Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
					Math.sin(dLon/2) * Math.sin(dLon/2)
				;
			var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
			var d = R * c; // Distance in km
			return d.toFixed(1);
		};

		function deg2rad(deg) {
			return deg * (Math.PI/180);
		}

		$scope.getRightIconForHelpRequest = function(tagToShow){

		return 'icon ' + tagToShow;
		};


})




.controller('ChatsCtrl', function($scope, Chats) {
	// With the new view caching in Ionic, Controllers are only called
	// when they are recreated or on app start, instead of every page change.
	// To listen for when this page is active (for example, to refresh data),
	// listen for the $ionicView.enter event:
	//
	//$scope.$on('$ionicView.enter', function(e) {
	//});
	console.log('chats loading');


	$scope.chats = Chats.all();
	$scope.remove = function(chat) {
		Chats.remove(chat);

	};
})

	.controller('ChatzCtrl', function($scope, Chats) {
		// With the new view caching in Ionic, Controllers are only called
		// when they are recreated or on app start, instead of every page change.
		// To listen for when this page is active (for example, to refresh data),
		// listen for the $ionicView.enter event:
		//
		//$scope.$on('$ionicView.enter', function(e) {
		//});
		console.log('chats loading');


		$scope.chats = Chats.all();
		$scope.remove = function(chat) {
			Chats.remove(chat);

		};
	})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
	$scope.chat = Chats.get($stateParams.chatId);
})


.controller('AdsCtrl', function($scope, $ionicActionSheet, AdMob, iAd) {

	$scope.manageAdMob = function() {

		// Show the action sheet
		var hideSheet = $ionicActionSheet.show({
			//Here you can add some more buttons
			buttons: [
				{ text: 'Show Banner' },
				{ text: 'Show Interstitial' }
			],
			destructiveText: 'Remove Ads',
			titleText: 'Choose the ad to show',
			cancelText: 'Cancel',
			cancel: function() {
				// add cancel code..
			},
			destructiveButtonClicked: function() {
				console.log("removing ads");
				AdMob.removeAds();
				return true;
			},
			buttonClicked: function(index, button) {
				if(button.text == 'Show Banner')
				{
					console.log("show banner");
					AdMob.showBanner();
				}

				if(button.text == 'Show Interstitial')
				{
					console.log("show interstitial");
					AdMob.showInterstitial();
				}

				return true;
			}
		});
	};

	$scope.manageiAd = function() {

		// Show the action sheet
		var hideSheet = $ionicActionSheet.show({
			//Here you can add some more buttons
			buttons: [
			{ text: 'Show iAd Banner' },
			{ text: 'Show iAd Interstitial' }
			],
			destructiveText: 'Remove Ads',
			titleText: 'Choose the ad to show - Interstitial only works in iPad',
			cancelText: 'Cancel',
			cancel: function() {
				// add cancel code..
			},
			destructiveButtonClicked: function() {
				console.log("removing ads");
				iAd.removeAds();
				return true;
			},
			buttonClicked: function(index, button) {
				if(button.text == 'Show iAd Banner')
				{
					console.log("show iAd banner");
					iAd.showBanner();
				}
				if(button.text == 'Show iAd Interstitial')
				{
					console.log("show iAd interstitial");
					iAd.showInterstitial();
				}
				return true;
			}
		});
	};
})

// FEED
//brings all feed categories
.controller('FeedsCategoriesCtrl', function($scope, $http) {
	$scope.feeds_categories = [];

	$http.get('feeds-categories.json').success(function(response) {
		$scope.feeds_categories = response;
	});
})

//bring specific category providers
.controller('CategoryFeedsCtrl', function($scope, $http, $stateParams) {
	$scope.category_sources = [];

	$scope.categoryId = $stateParams.categoryId;

	$http.get('feeds-categories.json').success(function(response) {
		var category = _.find(response, {id: $scope.categoryId});
		$scope.categoryTitle = category.title;
		$scope.category_sources = category.feed_sources;
	});
})

//this method brings posts for a source provider
.controller('FeedEntriesCtrl', function($scope, $stateParams, $http, FeedList, $q, $ionicLoading, BookMarkService) {
	$scope.feed = [];

	var categoryId = $stateParams.categoryId,
			sourceId = $stateParams.sourceId;

	$scope.doRefresh = function() {

		$http.get('feeds-categories.json').success(function(response) {

			$ionicLoading.show({
				template: 'Loading entries...'
			});

			var category = _.find(response, {id: categoryId }),
					source = _.find(category.feed_sources, {id: sourceId });

			$scope.sourceTitle = source.title;

			FeedList.get(source.url)
			.then(function (result) {
				$scope.feed = result.feed;
				$ionicLoading.hide();
				$scope.$broadcast('scroll.refreshComplete');
			}, function (reason) {
				$ionicLoading.hide();
				$scope.$broadcast('scroll.refreshComplete');
			});
		});
	};

	$scope.doRefresh();

	$scope.bookmarkPost = function(post){
		$ionicLoading.show({ template: 'Post Saved!', noBackdrop: true, duration: 1000 });
		BookMarkService.bookmarkFeedPost(post);
	};
})

// SETTINGS
.controller('SettingsCtrl', function($scope, $ionicActionSheet, $state) {
	$scope.airplaneMode = true;
	$scope.wifi = false;
	$scope.bluetooth = true;
	$scope.personalHotspot = true;

	$scope.checkOpt1 = true;
	$scope.checkOpt2 = true;
	$scope.checkOpt3 = false;

	$scope.radioChoice = 'B';

	// Triggered on a the logOut button click
	$scope.showLogOutMenu = function() {

		// Show the action sheet
		var hideSheet = $ionicActionSheet.show({
			//Here you can add some more buttons
			// buttons: [
			// { text: '<b>Share</b> This' },
			// { text: 'Move' }
			// ],
			destructiveText: 'Logout',
			titleText: 'Are you sure you want to logout? This app is awsome so I recommend you to stay.',
			cancelText: 'Cancel',
			cancel: function() {
				// add cancel code..
			},
			buttonClicked: function(index) {
				//Called when one of the non-destructive buttons is clicked,
				//with the index of the button that was clicked and the button object.
				//Return true to close the action sheet, or false to keep it opened.
				return true;
			},
			destructiveButtonClicked: function(){
				//Called when the destructive button is clicked.
				//Return true to close the action sheet, or false to keep it opened.
				$state.go('auth.walkthrough');
			}
		});

	};
})

// TINDER CARDS
.controller('TinderCardsCtrl', function($scope, $http) {

	$scope.cards = [];


	$scope.addCard = function(img, name) {
		var newCard = {image: img, name: name};
		newCard.id = Math.random();
		$scope.cards.unshift(angular.extend({}, newCard));
	};

	$scope.addCards = function(count) {
		$http.get('http://api.randomuser.me/?results=' + count).then(function(value) {
			angular.forEach(value.data.results, function (v) {
				$scope.addCard(v.picture.large, v.name.first + " " + v.name.last);
			});
		});
	};

	$scope.addFirstCards = function() {
		$scope.addCard("https://dl.dropboxusercontent.com/u/30675090/envato/tinder-cards/left.png","Nope");
		$scope.addCard("https://dl.dropboxusercontent.com/u/30675090/envato/tinder-cards/right.png", "Yes");
	};

	$scope.addFirstCards();
	$scope.addCards(5);

	$scope.cardDestroyed = function(index) {
		$scope.cards.splice(index, 1);
		$scope.addCards(1);
	};

	$scope.transitionOut = function(card) {
		console.log('card transition out');
	};

	$scope.transitionRight = function(card) {
		console.log('card removed to the right');
		console.log(card);
	};

	$scope.transitionLeft = function(card) {
		console.log('card removed to the left');
		console.log(card);
	};
})


// BOOKMARKS
.controller('BookMarksCtrl', function($scope, $rootScope, BookMarkService, $state) {

	$scope.bookmarks = BookMarkService.getBookmarks();

	// When a new post is bookmarked, we should update bookmarks list
	$rootScope.$on("new-bookmark", function(event){
		$scope.bookmarks = BookMarkService.getBookmarks();
	});

	$scope.goToFeedPost = function(link){
		window.open(link, '_blank', 'location=yes');
	};
	$scope.goToWordpressPost = function(postId){
		$state.go('app.post', {postId: postId});
	};
})

// WORDPRESS
.controller('WordpressCtrl', function($scope, $http, $ionicLoading, PostService, BookMarkService) {
	$scope.posts = [];
	$scope.page = 1;
	$scope.totalPages = 1;

	$scope.doRefresh = function() {
		$ionicLoading.show({
			template: 'Loading posts...'
		});

		//Always bring me the latest posts => page=1
		PostService.getRecentPosts(1)
		.then(function(data){
			$scope.totalPages = data.pages;
			$scope.posts = PostService.shortenPosts(data.posts);

			$ionicLoading.hide();
			$scope.$broadcast('scroll.refreshComplete');
		});
	};

	$scope.loadMoreData = function(){
		$scope.page += 1;

		PostService.getRecentPosts($scope.page)
		.then(function(data){
			//We will update this value in every request because new posts can be created
			$scope.totalPages = data.pages;
			var new_posts = PostService.shortenPosts(data.posts);
			$scope.posts = $scope.posts.concat(new_posts);

			$scope.$broadcast('scroll.infiniteScrollComplete');
		});
	};

	$scope.moreDataCanBeLoaded = function(){
		return $scope.totalPages > $scope.page;
	};

	$scope.bookmarkPost = function(post){
		$ionicLoading.show({ template: 'Post Saved!', noBackdrop: true, duration: 1000 });
		BookMarkService.bookmarkWordpressPost(post);
	};

	$scope.doRefresh();
})

// WORDPRESS POST
.controller('WordpressPostCtrl', function($scope, post_data, $ionicLoading) {

	$scope.post = post_data.post;
	$ionicLoading.hide();

	$scope.sharePost = function(link){
		window.plugins.socialsharing.share('Check this post here: ', null, null, link);
	};
})


.controller('ImagePickerCtrl', function($scope, $rootScope, $ionicPlatform, $cordovaCamera) {

	$scope.images = [];
	// $scope.image = {};

	// $scope.openImagePicker = function() {
	//
	// 	//We use image picker plugin: http://ngcordova.com/docs/plugins/imagePicker/
  //   //implemented for iOS and Android 4.0 and above.
	//
  //   $ionicPlatform.ready(function() {
  //     $cordovaImagePicker.getPictures()
  //      .then(function (results) {
  //         for (var i = 0; i < results.length; i++) {
  //           console.log('Image URI: ' + results[i]);
  //           $scope.images.push(results[i]);
  //         }
  //       }, function(error) {
  //         // error getting photos
  //       });
  //   });
	// };

	$scope.openImagePicker = function(){
    //We use image picker plugin: http://ngcordova.com/docs/plugins/imagePicker/
    //implemented for iOS and Android 4.0 and above.

    $ionicPlatform.ready(function() {
      var options = {
        quality: 100,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
        allowEdit: true,
        encodingType: Camera.EncodingType.JPEG,
        targetWidth: 350,
        targetHeight: 350,
        saveToPhotoAlbum: false
      };
      $cordovaCamera.getPicture(options)
       .then(function (imageData) {
          var image = "data:image/jpeg;base64," + imageData;
          $scope.images.push(image);
        }, function(error) {
          console.log(error);
        });
    });
  };

	$scope.removeImage = function(image) {
		$scope.images = _.without($scope.images, image);
	};

	$scope.shareImage = function(image) {
		window.plugins.socialsharing.share(null, null, image);
	};

	$scope.shareAll = function() {
		window.plugins.socialsharing.share(null, null, $scope.images);
	};
})

;
